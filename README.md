This is the implementation of Option 2, Load Monitoring Application

* I collect the system load average using /proc/uptime every 10s
* I display a graph of the system load average over the past 10 minutes using highcharts since I was already familiar with it
* I added a refresh button to re-render the chart and alerts with the newest data
* High and Low load messages (alerts) are generated per the specifications
* All alerts are allways shown
* A integration test covers 88% of the alert logic, 22% is simple error handling
* The integration tests also cover much of the point and alert create/get/filter logic

TODO/IMPROVEMENTS:
* Auto refresh web page option, 10s, 30s, 1m, etc...
* Pagination for Alerts
* Use a RDBMS such as postgres, I went with a file storage which is less setup for a "example" application
* Seperate the monitor/alert/point code more.  Would make unit testing possible along with simplifying the integration tests
* Use a http framework such as gorilla for the router/muxer.  Would remove some of the "extra" logic I had to add and make expanding easier
