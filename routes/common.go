package routes

import (
	"log"
	"net/http"

	"gitlab.com/christianmesh1/gomon/config"
)

func Serve(conf config.Config) {
	go func() {
		err := http.ListenAndServe(conf.BaseURL, nil)
		if err != nil {
			log.Print(err.Error())
		}
	}()
}
