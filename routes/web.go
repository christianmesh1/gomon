package routes

import (
	"log"
	"net/http"

	"gitlab.com/christianmesh1/gomon/config"
)

func SetupWEB(conf config.Config) {
	log.Printf("Starting WEB")
	http.Handle("/", http.FileServer(http.Dir("./routes/resources/")))
}
