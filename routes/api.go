package routes

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"gitlab.com/christianmesh1/gomon/config"
	"gitlab.com/christianmesh1/gomon/monitor"
)

func SetupAPI(conf config.Config) {
	log.Printf("Starting API")
	http.HandleFunc("/api", APIInfoHandler)
	http.HandleFunc("/api/data", APIDataHandler(conf))
	http.HandleFunc("/api/alert", APIAlertHandler(conf))
}

func APIInfoHandler(rw http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		rw.WriteHeader(http.StatusNotFound)
		return
	}

	type Info struct {
		Author  string `json:"author"`
		Version string `json:"version"`
	}

	i := Info{
		Author:  "Christian Mesh",
		Version: "0.0.1",
	}

	data, err := json.MarshalIndent(i, "", "    ")
	if err != nil {
		log.Printf("Unable to encode info: %v", err.Error())
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}

	rw.WriteHeader(http.StatusOK)
	rw.Write(data)
}

func APIDataHandler(conf config.Config) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		if r.Method != "GET" {
			rw.WriteHeader(http.StatusNotFound)
			return
		}

		start, err := timeFromURL("start", r)
		if err != nil {
			log.Printf("API Data Error: %v", err.Error())
			rw.WriteHeader(http.StatusBadRequest)
			return
		}
		end, err := timeFromURL("end", r)
		if err != nil {
			log.Printf("API Data Error: %v", err.Error())
			rw.WriteHeader(http.StatusBadRequest)
			return
		}

		points, err := monitor.GetData(start, end, conf.DataPath)
		if err != nil {
			log.Printf("API Monitor Data Error: %v", err.Error())
			rw.WriteHeader(http.StatusInternalServerError)
			return
		}

		data, err := json.Marshal(points)
		if err != nil {
			log.Printf("API Monitor Data Error: %v", err.Error())
			rw.WriteHeader(http.StatusInternalServerError)
			return
		}

		rw.WriteHeader(http.StatusOK)
		rw.Write(data)
	}
}

func timeFromURL(key string, r *http.Request) (t time.Time, err error) {
	value, ok := r.URL.Query()[key]
	if !ok {
		return t, fmt.Errorf("Missing url param %v", key)
	}
	return time.Parse(time.RFC3339, value[0])
}

func APIAlertHandler(conf config.Config) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		if r.Method != "GET" {
			rw.WriteHeader(http.StatusNotFound)
			return
		}

		points, err := monitor.AlertsFromFile(conf.DataPath)
		if err != nil {
			log.Printf("API Alert Error: %v", err.Error())
			rw.WriteHeader(http.StatusInternalServerError)
			return
		}

		data, err := json.Marshal(points)
		if err != nil {
			log.Printf("API Alert Error: %v", err.Error())
			rw.WriteHeader(http.StatusInternalServerError)
			return
		}

		rw.WriteHeader(http.StatusOK)
		rw.Write(data)
	}
}
