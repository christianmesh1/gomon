package monitor

import (
	"fmt"
	"time"

	"gitlab.com/christianmesh1/gomon/config"
)

func AlertIfNessesary(t time.Time, conf config.Config) error {
	data, err := GetData(t.Add(-2*time.Minute), t, conf.DataPath)
	if err != nil {
		return err
	}

	alerts, err := AlertsFromFile(conf.DataPath)
	if err != nil {
		return err
	}

	var sum float32
	for _, p := range data {
		sum += p.Load
	}
	avg := sum / float32(len(data))

	latestAlertType := AlertTypeLow
	if len(alerts) != 0 {
		latestAlertType = alerts[len(alerts)-1].Type
	}

	var a Alert
	a.Type = -1

	if avg > 1 {
		if latestAlertType == AlertTypeLow {
			a = NewAlert(t, AlertTypeHigh, fmt.Sprintf("High load generated an alert - load = %v, triggered at %v", avg, t))
		}
	} else {
		if latestAlertType == AlertTypeHigh {
			a = NewAlert(t, AlertTypeLow, fmt.Sprintf("Load back within normal levels - load = %v, triggered at %v", avg, t))
		}
	}

	if a.Type != -1 {
		err := a.Write(conf.DataPath)
		if err != nil {
			return fmt.Errorf("Error saving alert: %v", err.Error())
		}
	}

	return nil
}
