package monitor

import (
	"os"
	"path"
	"testing"
	"time"

	"gitlab.com/christianmesh1/gomon/config"
)

func pointsBack(ct time.Time, back int) time.Time {
	return ct.Add(time.Duration(-10*back) * time.Second)
}

func generatePointData(ct time.Time, load float32, quantity int) []Point {
	data := make([]Point, quantity)
	for i := 0; i < quantity; i++ {
		data[quantity-i-1] = Point{
			Time: pointsBack(ct, i),
			Load: load,
		}
	}
	return data
}

func TestAlertIfNessesary(t *testing.T) {
	type Case struct {
		Name   string
		Time   time.Time
		Points []Point
		Alerts []Alert
	}

	ct := time.Now().UTC().Truncate(time.Second)

	cases := []Case{
		{
			Name:   "No Data",
			Time:   ct,
			Points: []Point{},
			Alerts: []Alert{},
		},
		{
			Name:   "None => Low",
			Time:   ct,
			Points: generatePointData(ct, 0.5, 12),
			Alerts: []Alert{},
		},
		{
			Name:   "None => High",
			Time:   ct,
			Points: generatePointData(ct, 1.5, 12),
			Alerts: []Alert{{Time: pointsBack(ct, 12), Type: AlertTypeHigh}},
		},
		{
			Name:   "Low => Low",
			Time:   ct,
			Points: generatePointData(ct, 0.5, 24),
			Alerts: []Alert{},
		},
		{
			Name:   "Low => High",
			Time:   ct,
			Points: append(generatePointData(pointsBack(ct, 13), 0.9, 12), generatePointData(ct, 3.0, 12)...),
			Alerts: []Alert{{Time: pointsBack(ct, 12), Type: AlertTypeHigh}},
		},
		{
			Name:   "High => High",
			Time:   ct,
			Points: generatePointData(ct, 1.5, 24),
			Alerts: []Alert{{Time: pointsBack(ct, 12), Type: AlertTypeHigh}},
		},
		{
			Name:   "High => Low",
			Time:   ct,
			Points: append(generatePointData(pointsBack(ct, 13), 1.1, 12), generatePointData(ct, 0.1, 12)...),
			Alerts: []Alert{
				{Time: pointsBack(ct, 24), Type: AlertTypeHigh},
				{Time: pointsBack(ct, 12), Type: AlertTypeLow},
			},
		},
	}

	dir := path.Join(os.TempDir(), "gomon_test", time.Now().Format(time.RFC3339))
	t.Logf("Running tests in %v", dir)

	for _, c := range cases {
		t.Logf("Running %v", c.Name)

		err := os.MkdirAll(dir, 0755)
		if err != nil {
			t.Fatalf("Unable to create test data folder: %v", err.Error())
		}

		for _, p := range c.Points {
			err = WriteTimeData(p.Time, dir, p.Bytes())
			if err != nil {
				break
			}
			err = AlertIfNessesary(p.Time, config.Config{DataPath: dir})
			if err != nil {
				break
			}
		}
		if err != nil {
			t.Fatalf("Unable to create test data: %v", err.Error())
		}

		alerts, err := AlertsFromFile(dir)
		if err != nil {
			t.Fatal(err)
		}

		if actual, expected := len(alerts), len(c.Alerts); expected != actual {
			t.Errorf("Expected %v alerts, got %v", expected, actual)
		} else if len(alerts) > 1 {
			expected, actual := c.Alerts[0], alerts[0]
			if expected.Time != actual.Time || expected.Type != actual.Type {
				t.Errorf("Expected %v, got %v", expected, actual)
			}
		}

		err = os.RemoveAll(dir)
		if err != nil {
			t.Fatalf("Unable to remove test data: %v", err.Error())
		}
	}
}
