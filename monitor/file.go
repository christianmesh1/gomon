package monitor

import (
	"fmt"
	"io"
	"os"
	"path"
	"time"
)

func dirForTime(t time.Time) string {
	return t.Format("2006-01-02")
}

func fileForTime(t time.Time) string {
	return t.Format("15.data")
}

//File for every hour in a directory for each day
func PathForTime(t time.Time, prefix string) string {
	return path.Join(prefix, dirForTime(t), fileForTime(t))
}

// Writes the raw monitor data to a file/folder structure
// Assumes there are large gaps between writes
// Used https://golang.org/src/io/ioutil/ioutil.go?s=2520:2588#L66 as a template
func WriteTimeData(t time.Time, prefix string, data []byte) error {

	//Create dir if not exists
	err := os.MkdirAll(path.Join(prefix, dirForTime(t)), 0755)
	if err != nil {
		return err
	}

	//Open file to append
	f, err := os.OpenFile(PathForTime(t, prefix), os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		return err
	}

	//Write monitor data and close file
	n, err := f.Write(data)
	if err == nil && n < len(data) {
		err = io.ErrShortWrite
	}
	if err1 := f.Close(); err == nil {
		err = err1
	}
	return err
}

func GetData(start, end time.Time, path string) (res []Point, err error) {
	start = start.UTC()
	end = end.UTC()

	for curr := start; curr.Before(end.Add(time.Hour)); curr = curr.Add(time.Hour) {
		path := PathForTime(curr, path)

		_, err = os.Stat(path)

		if os.IsNotExist(err) {
			continue
		}
		if err != nil {
			return nil, err
		}

		data, err := PointsFromFile(path)
		if err != nil {
			return nil, fmt.Errorf("ERROR %v IS CORRUPT: %v", path, err.Error())
		}

		if curr == start {
			var filtered []Point
			for _, p := range data {
				if p.Time.After(start) || p.Time == start {
					filtered = append(filtered, p)
				}
			}
			data = filtered
		}

		if curr.After(end) || start.Add(time.Hour).After(end) {
			var filtered []Point
			for _, p := range data {
				if p.Time.Before(end) || p.Time == end {
					filtered = append(filtered, p)
				}
			}
			data = filtered
		}

		res = append(res, data...)

	}
	return res, nil
}
