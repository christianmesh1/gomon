package monitor

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path"
	"strconv"
	"time"
)

const (
	AlertTypeLow  = 0
	AlertTypeHigh = 1
)

func alertFilePath(prefix string) string {
	return path.Join(prefix, "alerts.data")
}

type Alert struct {
	Time    time.Time `json:"time"`
	Type    int       `json:"-"`
	Message string    `json:"message"`
}

func NewAlert(time time.Time, typeid int, msg string) Alert {
	return Alert{Time: time, Type: typeid, Message: msg}
}

func (a Alert) Bytes() []byte {
	s := fmt.Sprintf("%v %v %v\n", a.Time.Format(time.RFC3339), a.Type, a.Message)
	return []byte(s)
}

func (a Alert) Write(prefix string) error {
	//Create dir if not exists
	err := os.MkdirAll(prefix, 0755)
	if err != nil {
		return err
	}

	//Open file to append
	f, err := os.OpenFile(alertFilePath(prefix), os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		return err
	}

	data := a.Bytes()

	//Write monitor data and close file
	n, err := f.Write(data)
	if err == nil && n < len(data) {
		err = io.ErrShortWrite
	}
	if err1 := f.Close(); err == nil {
		err = err1
	}
	return err
}

func AlertsFromFile(path string) (alerts []Alert, err error) {
	_, err = os.Stat(alertFilePath(path))
	if os.IsNotExist(err) {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	f, err := os.Open(alertFilePath(path))
	if err != nil {
		return nil, err
	}
	defer f.Close()

	r := bufio.NewReader(f)

	for {
		var a Alert
		var ts string

		ts, err = r.ReadString(' ')
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}
		ts = ts[:len(ts)-1]

		typestr, err := r.ReadString(' ')
		if err != nil {
			return nil, err
		}

		typei, err := strconv.ParseInt(typestr[:len(typestr)-1], 10, 32)
		if err != nil {
			return nil, err
		}
		a.Type = int(typei)

		a.Message, err = r.ReadString('\n')
		if err != nil {
			return nil, err
		}
		a.Message = a.Message[:len(a.Message)-1]

		a.Time, err = time.Parse(time.RFC3339, ts)
		if err != nil {
			return nil, err
		}

		alerts = append(alerts, a)
	}
	return alerts, nil
}
