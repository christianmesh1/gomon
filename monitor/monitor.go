package monitor

import (
	"log"
	"sync"
	"time"

	"gitlab.com/christianmesh1/gomon/config"
)

type Monitor struct {
	conf   config.Config
	ticker *time.Ticker
	stop   sync.WaitGroup
}

func New(conf config.Config) *Monitor {
	return &Monitor{
		conf: conf,
	}
}

func (m *Monitor) Start() {
	log.Print("Starting Monitor")

	m.stop.Add(1)
	go m.run()
}

func (m *Monitor) Close() {
	log.Print("Closing Monitor")

	m.ticker.Stop()
	m.stop.Wait()
}

func (m *Monitor) run() {
	m.ticker = time.NewTicker(time.Second)

	for {
		timeout := time.After(time.Second * 2)

		select {
		case t := <-m.ticker.C:
			if t.Second()%10 == 0 {
				m.datapoint()
			}
		case <-timeout:
			log.Print("Monitor Closed")
			m.stop.Done()
		}
	}
}

func (m *Monitor) datapoint() {
	p, err := NewPoint()
	if err != nil {
		log.Printf("Unable to create point: %v", err.Error())
		return
	}

	err = WriteTimeData(p.Time, m.conf.DataPath, p.Bytes())
	if err != nil {
		log.Printf("Unable to write point: %v", err.Error())
	}

	err = AlertIfNessesary(p.Time, m.conf)
	if err != nil {
		log.Printf("Unable to handle alert process: %v", err.Error())
	}
}
