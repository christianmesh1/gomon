package monitor

import (
	"fmt"
	"io"
	"os"
	"time"
)

type Point struct {
	Time time.Time `json:"time"`
	Load float32   `json:"load"`
}

func NewPoint() (Point, error) {
	p := Point{Time: time.Now().UTC()}

	f, err := os.Open("/proc/loadavg")
	if err != nil {
		return p, err
	}
	defer f.Close()

	_, err = fmt.Fscanf(f, "%f", &p.Load)
	return p, err
}

func (p Point) Bytes() []byte {
	s := fmt.Sprintf("%v %v\n", p.Time.Format(time.RFC3339), p.Load)
	return []byte(s)
}

func PointsFromFile(path string) (points []Point, err error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	for {
		var p Point
		var ts string

		_, err = fmt.Fscanf(f, "%s %f\n", &ts, &p.Load)
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}

		p.Time, err = time.Parse(time.RFC3339, ts)
		if err != nil {
			return nil, err
		}

		points = append(points, p)
	}
	return points, nil
}
