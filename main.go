package main

import (
	"flag"
	"fmt"
	"log"

	"gitlab.com/christianmesh1/gomon/config"
	"gitlab.com/christianmesh1/gomon/monitor"
	"gitlab.com/christianmesh1/gomon/routes"
)

func main() {
	confFile := flag.String("conf", "./config.json", "Config file for this application")
	flag.Parse()

	conf := config.New()
	err := conf.Load(*confFile)
	if err != nil {
		log.Fatalf("Could not open config file: %v", err.Error())
	}

	mon := monitor.New(conf)
	if err != nil {
		log.Fatalf("Could not start monitor: %v", err.Error())
	}
	mon.Start()
	defer mon.Close()

	routes.SetupAPI(conf)
	routes.SetupWEB(conf)
	routes.Serve(conf)

	log.Print("Press enter to exit")
	var tmp string
	fmt.Scanf("%v", &tmp)
}
