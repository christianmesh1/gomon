package config

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

type Config struct {
	BaseURL  string `json:"base_url"`
	DataPath string `json:"data_path"`
}

func New() Config {
	return Config{
		BaseURL:  "localhost:8123",
		DataPath: "./data",
	}
}

func (c *Config) Load(filepath string) error {
	if _, err := os.Stat(filepath); err != nil {
		log.Printf("Unable to open config file: %v", err.Error())
		return nil
	}

	data, err := ioutil.ReadFile(filepath)
	if err != nil {
		return err
	}

	return json.Unmarshal(data, &c)
}
